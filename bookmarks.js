let bookmarks = {}, matchingBookmarks = {};
let searchString = '';

try {
  browser.bookmarks.search({}).then(b => {
    bookmarks = b.filter(bookmark => bookmark.type === 'bookmark');
  });
} catch (e) {
  chrome.bookmarks.search({}, b => {
    bookmarks = b;
  });
}

const isSearching = () => {
  return searchString.length > 0;
};

const openBookmark = (bookmark) => {
  location.href = bookmark.url;
};

const removeDuplicatesInList = (list) => {
  return list.filter((element, index) => {
    return list.indexOf(element) === index;
  });
};

const filterBookmarks = () => {
  let allMatches = [];

  const startsWithString = bookmarks.filter(bookmark => {
    return bookmark.title.toLowerCase().startsWith(searchString.toLowerCase());
  });

  const wordsStartWithString = bookmarks.filter(bookmark => {
    return bookmark.title.toLowerCase().split(' ')
        .map(word => word.startsWith(searchString.toLowerCase()))
        .find(match => match);
  });

  const containingString = bookmarks.filter(bookmark => {
    return bookmark.title.toLowerCase().includes(searchString.toLowerCase());
  });

  const containingStringInUrl = bookmarks.filter(bookmark => {
    const untilDoubleColonDoubleSlash = /(.*:\/\/)/;
    const result = bookmark.url.split(untilDoubleColonDoubleSlash);

    return result[result.length - 1].toLowerCase().includes(searchString.toLowerCase());
  });

  // First add more relevant matches
  allMatches = allMatches.concat(startsWithString);
  allMatches = allMatches.concat(wordsStartWithString);
  allMatches = allMatches.concat(containingString);
  allMatches = allMatches.concat(containingStringInUrl);

  return removeDuplicatesInList(allMatches);
};

const updateSearch = () => {
  const clockAndDate = document.getElementById('clock-and-date');
  const search = document.getElementById('bookmark-search');
  const results = document.getElementById('bookmark-results');

  const maxResultAmount = 4;

  search.textContent = searchString;

  search.style.opacity = isSearching() ? '1' : '0';
  results.style.opacity = isSearching() ? '1' : '0';
  clockAndDate.style.opacity = !isSearching() ? '1' : '0';

  while (results.hasChildNodes()) {
    results.removeChild(results.lastChild);
  }

  if (isSearching()) {
    matchingBookmarks = filterBookmarks().slice(0, maxResultAmount);

    matchingBookmarks.forEach(bookmark => {
      const result = document.createElement('div');
      result.textContent = bookmark.title;

      result.onclick = () => {
        openBookmark(bookmark);
      };

      results.appendChild(result);
    });
  } else {
    matchingBookmarks.length = 0;
  }
};

const bookmarkSearchOnKeydown = (e) => {
  if (e.key === 'Escape' || e.key === 'Esc') {
    searchString = '';
    updateSearch();
  } else if (e.key === 'Backspace') {
    searchString = searchString.substring(0, searchString.length - 1);
    updateSearch();
  } else if (e.key === 'Enter') {
    onEnterPress();
  } else if (e.key.length === 1) {
    searchString += e.key;
    updateSearch();
  }
};

const onEnterPress = () => {
  if (matchingBookmarks.length > 0) {
    openBookmark(matchingBookmarks[0]);
  } else if (searchString.length > 0) {
    location.href = settingSearchEnginePrefix() + encodeURIComponent(searchString);
  }
};
